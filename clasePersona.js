var persona=function(){
	//propiedad privada
	let nombre;
	//metodos publicos (el this lo hace publico)
	this.dormir=function(){
		console.log("zzzZZZZzzzzZZZ¡¡¡");
	}
	this.hablar=function(){
		console.log("BlavlaVLaBlabla..");
	}
	this.contar=function(){
		console.log("1,2,2,3,4,456,...");
	}
	this.adquirirNombre=function(nomb){
		nombre=nomb;
	}
	this.decirNombre=function(){
		console.log("Mi Nombre es "+nombre);
	}
};